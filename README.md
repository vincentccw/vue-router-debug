# TM Mobility Service Portal (MSP)

## ❗ Important: Best practices
- We are using `pnpm` for our package management. Please ❗ **install packages** with the `pnpm add` ❗ command.
- Use tab for indentation.
- In an effort to align our coding style with the direction the industry is taking, we are **dropping semicolon** in all our codes.
- Ensure trailing comma is always present, especially in multiline Object.
- Vue Composition API is highly encouraged. That said, with justifiably reason, you are welcome to use the Options API too.
    - Using the new `<script setup>` style is preferred. [Official docs here](https://v3.vuejs.org/api/sfc-script-setup.html).
- Typescript is optional.


### Build & Bundler

**Vite.js** — Our backbone build tool that enables us to use Vue and its ecosystem smoothly
https://vitejs.dev/guide/

### Frameworks & Libraries

- **Vitesse Lite** — Vite project starter template
https://github.com/antfu/vitesse-lite

- **Pinia** — State management for Vue that uses Vuex behind-the-scene
https://pinia.esm.dev/

- **Composition API Guide** — From the Vue team
https://v3.vuejs.org/guide/composition-api-introduction.html

- **VueUse** — A compilation of common utility function for Vue 3
https://vueuse.org/functions.html

- **Windi CSS** — Framework to work more efficiently with Tailwind
https://windicss.org/guide/

- **Iconify** — Our icon management framework
https://icon-sets.iconify.design/

### Project Features
#### Using Iconify with Auto Import
The name conversion for icons is:
```
{prefix}-{collection}-{icon}
```

Example usage:
```html
<icon-mdi-account />
<i-carbon-sun />
<i-carbon-moon />
```

In the case where icon has to be dynamic (i.e. using that with :is attribute), auto importing does not work.
It has to be manually imported. For e.g.
```html
<script setup>
import IconSun from '~icons/carbon/sun'
import IconMoon from '~icons/carbon/moon'

const isDayTime = ref(false)
</script>

<template>
	<component
		:is="isDayTime ? IconSun : IconMoon"
	/>
</template>
```

#### Path Aliasing

`~/` is aliased to `./src/` folder.

For example, instead of having

```ts
import { isUserLoggedIn } from '../../../stores/user'
```

You can use

```ts
import { isUserLoggedIn } from '~/stores/user'
```

#### File-based Routing

Routes will be auto-generated for Vue files in the `/src/pages`.
Check out [`vite-plugin-pages`](https://github.com/hannoeru/vite-plugin-pages) for details.

#### Components auto import
Uses `unplugin-vue-components`. [`Github page`](https://github.com/antfu/unplugin-vue-components).

Enables developer to use component in the template directly without having to import.

#### Vue Composition API auto import
Uses `unplugin-auto-import`. [`Github page`](https://github.com/antfu/unplugin-auto-import).

Enables developer to use Vue Composition APIs without having to import.
For e.g. you can directly use `computed(() => { ... })` in the code.

### Development Tools

- **PNPM** — Dependancy package management, replacing NPM
https://pnpm.io/

- **ESLint** (Note: this is not fully implemented yet)
https://eslint.org/

- **Typescript**
https://www.typescriptlang.org/



## First-time Setup
1. Have Node.js version 12 or higher installed.
   [Download here](https://nodejs.org/en/)
2. Make sure you have pnpm installed.
	`npm install -g pnpm`
3. Make sure your console is at the root directory of this project.
	`cd tm-msp`
4. Install the project dependancies.
	`pnpm i`


## Development
- Run the dev server: `npm run dev`



