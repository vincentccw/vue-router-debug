import path from 'path'
import { defineConfig } from 'vite'
import Vue from '@vitejs/plugin-vue'
import Pages from 'vite-plugin-pages'

import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import Components from 'unplugin-vue-components/vite'

import WindiCSS from 'vite-plugin-windicss'
import AutoImport from 'unplugin-auto-import/vite'

export default defineConfig({
	envDir: `./environment`,
	resolve: {
		alias: {
			'~/': `${path.resolve(__dirname, 'src')}/`,
			'~composition/': `${path.resolve(__dirname, 'src/logic/composition')}/`,
		},
	},
	plugins: [
		Vue(),

		// https://github.com/hannoeru/vite-plugin-pages
		Pages(),

		// https://github.com/antfu/unplugin-auto-import
		AutoImport({
			imports: [
				'vue',
				'vue-router',
				// '@vueuse/core',
			],
		}),

		// https://github.com/antfu/unplugin-vue-components
		Components({
			resolvers: [
				// auto import icons
				// https://github.com/antfu/unplugin-icons
				IconsResolver({
					componentPrefix: '',
				}),
			],
		}),

		// https://github.com/antfu/unplugin-icons
		Icons(),

		// https://github.com/antfu/vite-plugin-windicss
		WindiCSS(),
	],

	server: {
		fs: {
			strict: true,
		},
	},

	optimizeDeps: {
		include: ['vue', 'vue-router', '@vueuse/core'],
		exclude: ['vue-demi'],
	},
})
