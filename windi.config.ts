import { defineConfig } from 'windicss/helpers'
import colors from 'windicss/colors'

export default defineConfig({
	darkMode: 'class',
	// https://windicss.org/posts/v30.html#attributify-mode
	// attributify: true,
	theme: {
		extend: {
			colors: {
				'gray': colors.trueGray,
				'tm-orange': '#F09027',
				'tm-blue': '#0070c0',
			},
		},
	},
})
