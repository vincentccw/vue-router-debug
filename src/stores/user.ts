import { acceptHMRUpdate, defineStore } from 'pinia'
import { hashID } from '~/logic/helpers/uuid.js'

// interface User {
// 	id: string
// 	firstName: string
// 	lastName: string
// 	gender: string
// 	dob: string
// }


export const useUserStore = defineStore('user', {
	state() {
		return {
			userList: <User[]>[],
		}
	},
	actions: {
		createUser(userData) {
			this.userList.push({
				...userData,
				id: hashID.generateUnique(),
			})
		},
		addAgeToUser(userId: string) {
			const user = this.userList.find((user) => user.id === userId)
			const dateParts = user.dob.split('/')
			const dob = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0])
			dob.setFullYear(dob.getFullYear() - 1)
			user.dob = new Intl.DateTimeFormat('en-MY').format(dob)
		},
		deleteUser(userId: string) {
			this.userList.splice(this.userList.indexOf((user) => user.id === userId), 1)
		},
	},
	getters: {
		computedUserList(state) {
			return this.userList.map((user: User) => {
				const today = new Date()
				const age = today.getFullYear() - new Date(user.dob).getFullYear()
				const fullName = user.firstName + ' ' + user.lastName
				
				return {
					...user,
					age,
					fullName,
				}
			})
		},
	},
})

if (import.meta.hot) {
	import.meta.hot.accept(acceptHMRUpdate(useUserStore, import.meta.hot))
}
