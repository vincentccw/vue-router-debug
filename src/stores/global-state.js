import { createGlobalState, useStorage } from '@vueuse/core'

// default values
export const CreateGlobalState = createGlobalState(() => 
	useStorage('tm-global-state', {
		sideNavToggle: true,
		navigation: [
			{
				name:'dashboard',
				icon: 'FeHome',
				display:'home',
				url:'/dashboard',
				// active: false,
				subMenu: [],
			},
			{
				name:'dashboard-order-capture',
				icon: 'OrderCapture',
				display:'order capture',
				url:'/dashboard/order-capture',
				// active: false,
				subMenu: [
					{
						name:'dashboard-order-capture-5g-mobility',
						display:'5G mobility',
						url:'/dashboard/order-capture/5g-mobility',
						// active: false,
					},
					{
						name:'dashboard-order-capture-5g-mobility-aaaa',
						display:'aaaa',
						url:'/dashboard/order-capture/aaaa',
						// active: false,
					},
					{
						name:'dashboard-order-capture-lorem-ipsum-1',
						display:'lorem ipsum 1',
						url:'/dashboard/order-capture/lorem-ipsum-1',
						// active: false,
					},
					{
						name:'dashboard-order-capture-lorem-ipsum-2',
						display:'lorem ipsum 2',
						url:'/dashboard/order-capture/lorem-ipsum-2',
						// active: false,
					}
				],
			},
			{
				name:'dashboard-order-tracking',
				icon: 'FeSearch',
				display:'order tracking',
				url:'/dashboard/order-tracking',
				// active: false,
				subMenu: [],
			},
		]
	}),
)