import 'pinia'

export interface User {
	id: string
	firstName: string
	lastName: string
	gender: string
	dob: string
}
export interface UserComputed {
	id: string
	firstName: string
	lastName: string
	gender: string
	dob: string
	fullName: string
	age: number
}

declare module 'pinia' {
	export interface PiniaCustomStateProperties<S> {
		computedUserList: UserComputed[]
	}
}
