//import Vue from 'vue';
//import VueRouter from 'vue-router';

//Vue.use(VueRouter);

//const routes = [
//	{
//		path: '/',
//		redirect: '/login',
//	},
//	{
//		path: '/login',
//		name: 'Login',
//		component: Login,
//	},
//	// keep placeholder page seperate from rest of remote authentication pages
//	{
//		path: '/r',
//		name: 'RemoteAuthentication',
//		component: () => import('../views/remote-authentication/RemoteAuthentication.vue'),
//	},
//	{
//		path: '/remote-authentication',
//		name: 'RemoteAuthenticationWrapper',
//		// prevent access to /remote-authentication parent path
//		redirect: { name: '404' },
//		component: () => import('../views/remote-authentication/RemoteAuthenticationWrapper.vue'),
//		children: [
//			{
//				path: 'invalid',
//				name: 'RemoteAuthenticationInvalid',
//				component: () => import('../views/remote-authentication/Invalid.vue'),
//			},
//		],
//	},
//];

//const router = new VueRouter({
//	routes,
//});

//export default router;