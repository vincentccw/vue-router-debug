import axios from 'axios'
import lodash from 'lodash'
const API_BASE_URL = import.meta.env.VITE_API_BASE_URL
const API_URL_PREFIX = import.meta.env.VITE_API_URL_PREFIX

export const Status = {
	IDLE: 'IDLE',
	RUNNING: 'RUNNING',
	SUCCESS: 'SUCCESS',
	ERROR: 'ERROR',
}

const HTTP_METHODS = [
	'GET',
	'POST',
	'DELETE',
	'OPTIONS',
	'HEAD',
	'PUT',
	'PATCH',
]

export default function useAPIRequest(_directive, _submitData = null, _options = {}) {
	if (!_directive) throw new TypeError('First argument must be a string')
	
	let url = _directive
	let method = 'GET'
	let directiveSplit = _directive.split(' ')
	
	if (HTTP_METHODS.includes(directiveSplit[0])) {
		method = directiveSplit[0]
		url = directiveSplit[1]
	}
	
	const options = lodash.merge({
		axiosConfig: {
			baseURL: API_BASE_URL + `/${API_URL_PREFIX}`,
			url,
			method,
			headers: {
				Accept: 'application/json',
			},
		},
	}, _options)
	
	const status = ref(Status.IDLE)
	const response = ref({})
	const responseData = ref(null)
	
	const _resolveSubmitData = (_dataArg) => {
		let final = null
		const initData = unref(_submitData)
		const overwriteData = unref(_dataArg)
		
		if (initData && (typeof initData === 'object')) {
			final = initData
		}
		if (overwriteData && (typeof overwriteData === 'object')) {
			final = overwriteData
		}
		
		return final ? { data: final } : {}
	}
	
	const fetch = async (data = null) => {
		status.value = Status.RUNNING
		try {
			const resp = await axios.request({
				...options.axiosConfig,
				...(_resolveSubmitData(data)),
			})
			response.value = resp
			responseData.value = resp.data
			status.value = Status.SUCCESS
			return resp
		} catch (err) {
			status.value = Status.ERROR
			if (err.response) {
				console.log('err.response = ', err.response);
				response.value = err.response
				responseData.value = err.response.data
			}
			throw err
		}
	}
	
	return {
		response: readonly(response),
		responseData: readonly(responseData),
		fetch,
		status: readonly(status),
	}
}
