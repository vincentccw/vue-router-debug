import { ref, computed, readonly } from 'vue'

export default function useStopWatch(_options = {}) {
	
	const options = {
		interval: 1000,
		..._options,
	}
	
	let _intervalId = null
	const isPaused = ref(false)
	const isDestroyed = ref(false)
	
	const tickCount = ref(0)
	const elapsed = computed(() => {
		return tickCount.value * options.interval
	})
	
	const onTickFns = []
	
	const _getErrorDestroyed = () => {
		return new Error('This instance of useStopWatch() has been destroyed')
	}
	const _getErrorStarted = () => {
		return new Error('This instance of useStopWatch() has already started')
	}
	
	const onTick = (fn) => {
		if (isDestroyed.value) throw _getErrorDestroyed()
		onTickFns.push(fn)
		const untick = () => onTickFns.splice(onTickFns.indexOf(fn), 1)
		return untick
	}
	const start = () => {
		if (isDestroyed.value) throw _getErrorDestroyed()
		if (_intervalId) throw _getErrorStarted()
		
		_intervalId = setInterval(() => {
			if (isPaused.value) return
			tickCount.value += 1
			onTickFns.forEach(fn => fn({
				tickCount: tickCount.value,
				elapsed: elapsed.value,
			}))
		}, options.interval);
	}
	const reset = () => {
		if (isDestroyed.value) throw _getErrorDestroyed()
		tickCount.value = 0
	}
	const pause = () => {
		if (isDestroyed.value) throw _getErrorDestroyed()
		isPaused.value = true
	}
	const resume = () => {
		if (isDestroyed.value) throw _getErrorDestroyed()
		isPaused.value = false
	}
	const destroy = () => {
		if (isDestroyed.value) throw _getErrorDestroyed()
		clearInterval(_intervalId)
		isDestroyed.value = true
		onTickFns.length = 0 // quick way to clear all values in Array without assigning new value
	}
	
	return {
		onTick,
		tickCount: readonly(tickCount),
		isDestroyed: readonly(isDestroyed),
		elapsed,
		start,
		reset,
		pause,
		resume,
		isPaused,
		destroy,
	}
}
