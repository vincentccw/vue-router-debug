import lodash from 'lodash'
import useStopWatch from '~composition/useStopWatch.js'
import { whenever } from '@vueuse/core'

export default function useSessionTimeout(_options = {}) {
	
	const options = {
		interval: 1000,
		..._options,
		triggers: (_options.triggers || []).sort((a, b) => (a.elapse - b.elapse)).map(t => {
			return {
				...t,
				hasTriggered: false,
			}
		}),
	}
	console.log('options = ', options);
	
	if (options.triggers.length === 0) throw new Error('Must define at least one trigger for useSessionTimeout')
	
	// const { start, isPaused, onTick, pause, resume, reset, tickCount, elapsed } = useStopWatch({ interval: options.interval })
	const stopWatch = useStopWatch({ interval: options.interval })
	
	const nextTrigger = ref(options.triggers[0])
	const isPaused = ref(false)
	
	const reset = () => {
		stopWatch.reset()
		options.triggers.forEach((t) => {
			t.hasTriggered = false
		})
		nextTrigger.value = options.triggers[0]
	}
	const pause = () => {
		isPaused.value = true
		stopWatch.pause()
	}
	const resume = () => {
		isPaused.value = false
		stopWatch.resume()
	}
	
	
	const initListeners = () => {
		document.addEventListener('click', reset, { capture: true, passive: true })
		document.addEventListener('keydown', reset, { capture: true, passive: true })
	}
	const destroyListeners = () => {
		document.removeEventListener('click', reset, { capture: true, passive: true })
		document.removeEventListener('keydown', reset, { capture: true, passive: true })
	}
	
	const destroySelf = () => {
		destroyListeners()
		stopWatch.destroy()
	}
	
	const startWhenever = () => {
		const stopWhenever = whenever(
			() => (stopWatch.elapsed.value >= nextTrigger.value.elapse),
			() => {
				nextTrigger.value.handler({ reset, pause, resume, isPaused })
				nextTrigger.value.hasTriggered = true
				
				const nextCall = options.triggers.find(t => t.hasTriggered === false)
				
				if (nextCall) {
					nextTrigger.value = nextCall
				} else {
					// no more next call meaning we have triggered everything
					stopWhenever()
					destroySelf()
				}
			},
		)
	}
	
	
	stopWatch.start();
	initListeners()
	startWhenever()
	
	// onTick(({ tickCount, elapsed}) => {
		
	// })
	
	
	return {
		tickCount: stopWatch.tickCount,
		elapsed: stopWatch.elapsed,
		isPaused,
		reset,
	}
}
