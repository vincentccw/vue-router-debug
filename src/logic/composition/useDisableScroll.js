

export default function useDisableScroll (visibilityRef) {
	const rootEl = document.documentElement
	// const bodyEl = document.body
	
	const clearOverflowStyling = () => {
		rootEl.style.overflowY = rootEl.getAttribute('data-overflow-y-value');
		rootEl.removeAttribute('data-overflow-y-value');
		// bodyEl.style.overflowY = bodyEl.getAttribute('data-overflow-y-value')
		// bodyEl.removeAttribute('data-overflow-y-value')
	}
	
	const setOverflowHiddenStyling = () => {
		if (!rootEl.getAttribute('data-overflow-y-value')) {
			rootEl.setAttribute('data-overflow-y-value', window.getComputedStyle(rootEl).getPropertyValue('overflow-y'));
		}
		rootEl.style.overflowY = 'hidden';
		// if (!bodyEl.getAttribute('data-overflow-y-value')) {
		// 	bodyEl.setAttribute('data-overflow-y-value', window.getComputedStyle(bodyEl).getPropertyValue('overflow-y'))
		// }
		// bodyEl.style.overflowY = 'hidden'
	}
	
	// onMounted(() => {
	// 	setOverflowHiddenStyling()
	// })
	
	onUnmounted(() => {
		clearOverflowStyling()
	})
	
	watch(visibilityRef, (newValue) => {
		if (newValue === false) {
			clearOverflowStyling()
		} else {
			setOverflowHiddenStyling()
		}
	}, { immediate: true })
	
}

