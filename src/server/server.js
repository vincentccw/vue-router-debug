import { createServer, Model, Response } from 'miragejs'
const API_BASE_URL = import.meta.env.VITE_API_BASE_URL
const API_URL_PREFIX = import.meta.env.VITE_API_URL_PREFIX

const mock_userData = [
	{ userId: 'a', password: 'b' },
	{ userId: 'Avatar', password: 'lastAirbender' },
	{ userId: 'Korra', password: 'masterOf4Elements' },
	{ userId: 'Katara', password: 'Katara4eva' },
]

export function makeServer({ environment = 'development' } = {}) {
	const server = createServer({
		environment,
		
		models: {
			user: Model,
		},
		
		seeds(server) {
			server.create('user', { name: 'Bob' })
			server.create('user', { name: 'Alice' })
		},
		
		routes() {
			this.urlPrefix = API_BASE_URL
			this.namespace = `/${API_URL_PREFIX}`
			
			this.get('/users', (schema, request) => {
				return schema.users.all()
			})
			
			this.post('/token', (schema, request) => {
				// console.log('schema = ', schema)
				// console.log('request = ', request)
				const payload = JSON.parse(request.requestBody)
				
				if (!payload || (!payload.userId || !payload.password)) {
					return new Response(401, {}, {
						code: '101',
						description: 'Empty user id or password.',
						reason: 'Empty user id or password.',
					})
				}
				
				const { userId, password } = payload
				
				if (mock_userData.find( (item) => (item.userId === userId && item.password === password) )) {
					return {
						sessionToken: 'a6c39523b010016b64996ea30c6b72db024ed2a5cf80a11000c23a17e2c062f6',
						refreshToken: 'REFRESH_14879ufb983hfd822jg456585kceo29',
						sessionTimeout: 60,
					}
				} else {
					return new Response(401, {}, {
						code: '100',
						description: 'Invalid user id or password.',
						reason: 'Invalid user id or password.',
					})
				}
			})
		},
	})

	return server
}

