// register vue composition api globally
import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import routes from 'virtual:generated-pages'
import App from './App.vue'
import { createPinia } from 'pinia'
import { makeServer } from './server/server.js'


// windicss layers
import 'virtual:windi-base.css'
import 'virtual:windi-components.css'
import './styles/main.scss'
import 'virtual:windi-utilities.css'


if (process.env.NODE_ENV === 'development') {
	const USE_MIRAGE = import.meta.env.VITE_USE_MIRAGE
	if (USE_MIRAGE) makeServer()
}

const app = createApp(App)

app.use(createPinia())

app.directive('focus', {
	mounted(el) {
		el.focus()
	},
})

const router = createRouter({
	history: createWebHistory(),
	routes,
	linkActiveClass: 'active',
	linkExactActiveClass: 'active-exact',
})
app.use(router)
app.mount('#app')
